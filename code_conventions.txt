---------------
scan.number.set
---------------

set to zero at the end of every scan!
(note that this causes DScanContinuous to loop one extra time, changes in scan.number.set cause it to run)



---------
shut down
---------

ensure that all drivers reset AO and DO to zero on shutdown. 



-----------------
Hardware channels
-----------------

all hardware channels are coded as variables (daq_ch.XXX or com_ch.XXX)
***this change is incomplete***